package com.stockable.websrapper.scrapingAgent;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.stockable.webscrapper.entity.Holding;

public class Testing {
	public static String driverPath = "D:/Sandipan/Stockable/";
	public static WebDriver driver;
	public static void main(String arg[]) throws ParseException{

		driver = new ChromeDriver();
		driver.navigate().to("http://www.morningstar.in/mutualfunds/f0gbr06rl1/uti-mastershare-unit-growth/detailed-portfolio.aspx");


		Test();
	}
	private static void Test() throws ParseException {


		List<WebElement> holdingList = driver.findElement(By.className("pf_detailed"))
				.findElement(By.tagName("tbody")).findElements(By.tagName("tr"));

		List<Holding> allHoldingList = new ArrayList<Holding>();
		boolean isScrappingRequired = false;
		boolean isExited = false;
		String type = "NORMAL";
		for(WebElement eachHolding:holdingList){

			if(eachHolding.getText().equals("Stock")){
				isScrappingRequired =  true;
				continue;
			}

			if(eachHolding.getText().contains("Total Stock")){
				isScrappingRequired = false;
				continue;
			}

			if(eachHolding.getText().contains("New addition to portfolio")){
				isScrappingRequired = true;

				isExited = false;
				type = "NEWLY_ADDED";
				continue;
			}
			if(eachHolding.getText().contains("Exited stocks")){
				isScrappingRequired = true;
				isExited = true;
				type = "EXITED";
				continue;
			}


			if(isScrappingRequired){
				String stockName = eachHolding.findElements(By.tagName("td")).get(1).getText().trim();
				String sector = eachHolding.findElements(By.tagName("td")).get(3).getText().trim();
				String weightage = eachHolding.findElements(By.tagName("td")).get(5).getText().trim();
				String marketValue = eachHolding.findElements(By.tagName("td")).get(6).getText().trim();
				String quantity = eachHolding.findElements(By.tagName("td")).get(7).getText().trim();
				String previousQuantity = eachHolding.findElements(By.tagName("td")).get(8).getText().trim();
				String firstBoughtDate = eachHolding.findElements(By.tagName("td")).get(9).getText().trim();

				System.out.println("stockName:"+stockName);
				System.out.println("sector:"+sector);
				System.out.println("weightage:"+weightage);
				System.out.println("marketValue:"+marketValue);
				System.out.println("quantity:"+quantity);
				System.out.println("previousQuantity:"+previousQuantity);
				System.out.println("firstBoughtDate:"+firstBoughtDate);
				System.out.println("-------------------------------");

				Holding holding = new Holding();
				if(isExited){
					holding.setStockName(stockName);
					holding.setMarketValue(Double.parseDouble(marketValue));
					holding.setPrevQuantity(Integer.parseInt(previousQuantity));
				}
				else{
					holding.setStockName(stockName);

					holding.setSector(sector);
					holding.setWeightage(Double.parseDouble(weightage));
					holding.setMarketValue(Double.parseDouble(marketValue));
					holding.setQuantity(Integer.parseInt(quantity));
					holding.setPrevQuantity(Integer.parseInt(previousQuantity));
					if(firstBoughtDate.indexOf("-")<0){
						holding.setFirstBoughtDate(firstBoughtDate);
					}
				}
				holding.setType(type);

				allHoldingList.add(holding);
			}
		}

		System.out.println("holding size:"+allHoldingList.size());
		//fund.setHoldingList(allHoldingList);

		//	return fund;

	}
}
