package com.stockable.webscrapper.entity;

import java.util.List;

public class MorningStar {

	public List<MutualFund> morningStarDetails;

	public List<MutualFund> getMorningStarDetails() {
		return morningStarDetails;
	}

	public void setMorningStarDetails(List<MutualFund> morningStarDetails) {
		this.morningStarDetails = morningStarDetails;
	}


}
